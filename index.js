
db.rooms.insertOne({
	"name": "Single",
	"accomodate": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessitites",
	"rooms_available": 10,
	"isAvailable": false
});



db.rooms.insertMany([
		{
			"name": "double",
			"accomodate": 3,
			"price": 2000,
			"description": "A room fir for a small family going on a vacation",
			"rooms_available": 5,
			"isAvailable": false
		},
		{
			"name": "queen",
			"accomodate": 4,
			"price": 4000,
			"description": "A simple room with all the basic necessitites",
			"rooms_available": 15,
			"isAvailable": false
		},


	]);

db.rooms.find({
	"name": "double"
});

db.rooms.updateOne(
	{"name": "queen"},
	{$set:{"rooms_available": 0}}
);

db.rooms.deleteOne({"rooms_available": 0});